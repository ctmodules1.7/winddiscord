package ru.will0376.winddiscord;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import ru.will0376.windevents.events.EventCheckTokenFailed;
import ru.will0376.windevents.events.EventPrintAnswer;
import ru.will0376.windevents.events.EventPrintHelp;
import ru.will0376.windevents.events.EventReloadConfig;
import ru.will0376.windevents.utils.ChatForm;
import ru.will0376.windevents.utils.Logger;
import ru.will0376.windevents.utils.Utils;

public class Events {
	@SubscribeEvent
	public void printToDiscordEvent(EventPrintAnswer e) {
		if (WDiscord.config.isEnabled()) {
			if (e.isReport()) {
				String tmp = String.format("Жалоба от: %s,\nНа игрока: %s,\nСсылка: %s", e.getAdminNick(), e.getPlayerNick(), e.getText());
				Discord.printToDiscordChannel(tmp);
			} else if (!e.getAdminNick().equals("Server")
					|| (e.getAdminNick().equals("Server") && WDiscord.config.isPrintRequestFromConsole())
					|| (e.getAdminNick().equals("daemon") && WDiscord.config.isPrintRequestFromDaemon())) {
				String tmp = String.format("Screen from %s\nby admin %s\nwith link %s", e.getPlayerNick(), e.getAdminNick(), e.getText());
				Discord.printToDis(tmp);
			}
		}
	}

	@SubscribeEvent
	public void catchCheckingToken(EventCheckTokenFailed e) {
		if (WDiscord.config.isEnabled()) {
			if (e.getAdminNick().equals(WDiscord.config.getBotName()))
				Discord.printToDis(String.format("[Discord_token] Error! Player %s has invalid token: %s", e.getPlayerNick(), e.getToken()));
		}
	}

	@SubscribeEvent
	public void printHelp(EventPrintHelp e) {
		Utils.printWithCase(e.getPlayer(), ChatForm.prefix, Utils.fromCaseToList(Utils.makeUsage(WDiscord.MOD_NAME, Commands.usage, Commands.aliases, Commands.permissions), "\n"));
	}

	@SubscribeEvent
	public void reload(EventReloadConfig e) {
		WDiscord.config.launch();
		Discord.restartBot();
		if (Utils.getEPMP(e.getSender()) == null) Logger.log(1, "[WDiscord]", "Reloaded");
		else
			Utils.getEPMP(e.getSender()).addChatMessage(new ChatComponentText(EnumChatFormatting.GOLD + "[WDiscord] Reloaded"));
	}

}
