package ru.will0376.winddiscord;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;
import ru.will0376.windevents.utils.ChatForm;
import ru.will0376.windevents.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Commands extends CommandBase {
	public static String usage =
			" /ctmd printToDis\n" +
					" /ctmd restartDiscordBot\n" +
					" /ctmd stopDiscordBot\n" +
					" /ctmd permissions";
	public static String aliases =
			" Aliases: [ctmdis, ctm-dis, ctm-d, ctmd, ctmdiscord]";
	public static String permissions =
			" Permissions: [\n" +
					"   ctmodules.report.admin.printToDis\n" +
					"   ctmodules.report.admin.restartdiscrodbot\n" +
					"   ctmodules.report.admin.stopDiscordBot\n" +
					" ]";

	@Override
	public String getCommandName() {
		return "ctm-discord";
	}

	@Override
	public String getCommandUsage(ICommandSender sender) {
		return usage;
	}

	@Override
	public void processCommand(ICommandSender sender, String[] args) throws CommandException {
		execute(sender, args);
	}

	@Override
	public List getCommandAliases() {
		ArrayList<String> al = new ArrayList<>();
		al.add("ctmdis");
		al.add("ctm-dis");
		al.add("ctm-d");
		al.add("ctmd");
		al.add("ctmdiscord");
		return al;
	}

	@Override
	public List addTabCompletionOptions(ICommandSender sender, String[] args) {
		if (args.length == 1)
			return getListOfStringsMatchingLastWord(args, "printToDis", "restartDiscordBot", "stopDiscordBot", "permissions");
		else {
			return args.length >= 2 ? getListOfStringsMatchingLastWord(args, Utils.getAllPlayers().stream().map(e -> e.getDisplayName()).toArray(String[]::new)) : Collections.emptyList();
		}
	}

	public void execute(ICommandSender sender, String[] args) throws CommandException {
		if (args[0].equalsIgnoreCase("printToDis")) {
			if (!Utils.checkPermission(sender, "ctmodules.admin.printToDis")) {
				return;
			}
			StringBuilder tmp = new StringBuilder();
			for (String tt : args)
				tmp.append(" ").append(tt);
			tmp = new StringBuilder(tmp.toString().replace(args[0], ""));
			Discord.printToDis(tmp.toString());
		} else if (args[0].equalsIgnoreCase("restartDiscrodBot")) {
			if (!Utils.checkPermission(sender, "ctmodules.discord.admin.restartdiscrodbot")) {
				return;
			}
			Discord.killbot();
			Discord.DiscordThread();
			sender.addChatMessage(new ChatComponentText(ChatForm.prefix + "Done!"));

		} else if (args[0].equalsIgnoreCase("stopDiscordBot")) {
			if (!Utils.checkPermission(sender, "ctmodules.discord.admin.stopDiscordBot")) {
				return;
			}
			Discord.killbot();
			sender.addChatMessage(new ChatComponentText(ChatForm.prefix + "Done!"));
		} else if (args[0].equalsIgnoreCase("permissions")) {
			Utils.printWithCase(sender, ChatForm.prefix, Utils.fromCaseToList(permissions, "\n"));
			//sender.sendMessage(new ChatComponentText(ChatForm.prefix + "Permissions:\n" + permissions));
		} else {
			Utils.printWithCase(sender, ChatForm.prefix, Utils.fromCaseToList(usage, "\n"));
			//sender.sendMessage(new ChatComponentText(ChatForm.prefix + usage));
		}

	}


}
