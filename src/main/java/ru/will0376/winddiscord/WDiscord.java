package ru.will0376.winddiscord;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.common.MinecraftForge;
import ru.will0376.windevents.events.EventLogPrint;
import ru.will0376.windevents.events.EventModStatusResponse;
import ru.will0376.windevents.utils.Logger;

import java.util.List;

@Mod(
		modid = WDiscord.MOD_ID,
		name = WDiscord.MOD_NAME,
		version = WDiscord.VERSION,
		acceptableRemoteVersions = "*",
		dependencies = "required-after:windevents@[1.0,)"

)
public class WDiscord {
	public static final String MOD_ID = "wdiscord";
	public static final String MOD_NAME = "WDiscord";
	public static final String VERSION = "1.0";
	public static Config config;
	@Mod.Instance(MOD_ID)
	public static WDiscord INSTANCE;

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {
		config = new Config(event.getSuggestedConfigurationFile());
		config.launch();
		FMLCommonHandler.instance().bus().register(new Events());
	}

	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent e) {
		MinecraftForge.EVENT_BUS.post(new EventLogPrint(3, EnumChatFormatting.GOLD + "[Ctdiscord]" + EnumChatFormatting.RESET, "PostInit done!"));
		MinecraftForge.EVENT_BUS.post(new EventModStatusResponse(MOD_ID, config.isEnabled()));
	}

	@Mod.EventHandler
	public void startingServer(FMLServerStartingEvent e) {
		Discord.DiscordThread();
		e.registerServerCommand(new Commands());
	}

	public boolean checkOnWhitelist(String nick) {
		Logger.log(3, "[Ctdiscord.main]", "Checking on Whitelist nick: " + nick);
		List<String> whitelist = config.getWhitelist();
		for (String s : whitelist)
			if (s.equals(nick))
				return false;
		return true;
	}

}
